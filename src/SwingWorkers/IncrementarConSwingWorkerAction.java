package SwingWorkers;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.SwingWorker;

/**
 * @author pacoaldarias <paco.aldarias@ceedcv.es>
 */
public class IncrementarConSwingWorkerAction extends AbstractAction {

  private EjemploFrame ejemploFrame;

  IncrementarConSwingWorkerAction(
          EjemploFrame ejemploSinSwingWorkerFrame) {
    this.ejemploFrame = ejemploSinSwingWorkerFrame;
  }

  @Override
  public void actionPerformed(ActionEvent arg0) {
    final SwingWorker worker = new SwingWorker() {
      @Override
      protected Object doInBackground() throws Exception {
        int ite = 0;
        while (ite < EjemploFrame.MAX_ITE) {
          ite = ite + 1;
          ejemploFrame.getTextField().setText("" + ite);
        }
        return null;
      }
    };
    worker.execute();
  }
}
